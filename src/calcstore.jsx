import { create } from 'zustand';

const useZStore = create((set) => ({
  value: 1, // initial value
  multiply: (factor) => set((state) => ({ value: state.value * factor }))
}));

export default useZStore;

