import { immer } from 'zustand/middleware/immer';
import { useEffect } from 'react';
import { create } from 'zustand';

// Login Store
const useLoginStore = create(
      immer((set,get) => ({
            isLoggedIn: false,
            userData: null,

            login: () => {
                  set((state) => {
                        state.isLoggedIn = true;
                        state.userData = {
                              name: 'John Doe',
                              email: 'johndoe@example.com',
                        };
                  });

                  console.log("login", get().userData);
            },

            logout: () => {
                  set((state) => {
                        state.isLoggedIn = false;
                        state.userData = null;
                  });
                  console.log("logout");
            },
      }))
);

// User Data Store
const useUserDataStore = create(
      immer((set, get) => ({

            altUserData: null,

            alterUserData: () => {
                  const loginStore = get(useLoginStore);

                  set((state) => {
                        if (loginStore.isLoggedIn) {
                              //state.userData.name = loginStore.userData.name + ' Modified';
                              state.altUserData = {
                                    ...loginStore.userData,
                              }
                        }
                  });
            },

            getUserData: () => {
                  const loginStore = get(useLoginStore);

                  return loginStore.isLoggedIn ? loginStore.userData : null;
            },
      }))
);

// Component
const UserComponent = () => {
      const loginStore = useLoginStore();
      const userDataStore = useUserDataStore();

      useEffect(() => {
            // Simulate login on component mount
            loginStore.login();
      }, []);

      const handleAlterUserData = () => {
            userDataStore.alterUserData();
      };

      //const userData = userDataStore.getUserData();
      const { userData } = useUserDataStore((state) => state);

      return (
            <div>
                  {userData ? (
                        <div>
                              <h2>Welcome, {userData.name}!</h2>
                              <p>Email: {userData.email}</p>
                        </div>
                  ) : (
                        <p>Please log in</p>
                  )}

                  <button onClick={handleAlterUserData}>Alter User Data</button>
                  <button onClick={loginStore.logout}>Logout</button>
            </div>
      );
};

export default UserComponent;
